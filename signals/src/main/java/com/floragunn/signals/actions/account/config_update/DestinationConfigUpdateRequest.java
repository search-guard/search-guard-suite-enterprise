package com.floragunn.signals.actions.account.config_update;


import org.elasticsearch.action.support.nodes.BaseNodesRequest;

public class DestinationConfigUpdateRequest extends BaseNodesRequest<DestinationConfigUpdateRequest> {

    public DestinationConfigUpdateRequest() {
        super((String[]) null);
    }

}
